/*Copyright [2021] [Serena Blanco García]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.
See the License for the specific language governing permissions
and
limitations under the License.*/

package dominio;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Esta clase es responsable de leer el tablero de un fichero en forma de ceros
 * y unos, ir transitando de estados e ir mostrando dichos estados.
 */
public class Tablero {
    private static int DIMENSION = 32;
    private int[][] estadoActual; // matriz que representa el
    // estado actual.
    private int[][] estadoSiguiente = new int[DIMENSION][DIMENSION]; // Matriz que representa el estado siguiente.

    public Tablero() {
        estadoActual = new int[DIMENSION][DIMENSION];
    }

    /**
     * Lee el estado inicial de un fichero llamado ‘matriz‘.
     * 
     * Complejidad temporal: O(n^2)
     * Complejidad espacial: O(1)
     */
    public void leerEstadoActual() {
            FileReader ficheroLectura;
            try {
                ficheroLectura = new FileReader("matriz.txt");
                Scanner input = new Scanner(ficheroLectura);
                for(int i=0; i<DIMENSION; i++){
                    String linea = input.nextLine();
                    for(int j=0; j<DIMENSION; j++){
                        estadoActual[i][j] = Character.getNumericValue(linea.charAt(j));
                    }
                }
            input.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
    }

    // La secuencia de ceros y unos del fichero es guardada en ‘estadoActual‘ y, utilizando las reglas del juego
    // de la vida, se insertan los ceros y unos correspondientes en ‘estadoSiguiente‘.
    /**
     * Genera un estado inicial aleatorio. Para cada celda genera un número
     * aleatorio en el intervalo [0, 1). Si el número es menor que 0,5, entonces la
     * celda está inicialmente viva. En caso contrario, está muerta.
     * 
     * Complejidad temporal: O(n^2)
     * Complejidad espacial: O(1)
     */
    public void generarEstadoActualPorMontecarlo() {
        for (int i = 1; i < DIMENSION-1; i++) {
            for (int j = 1; j < DIMENSION-1; j++) {
                if (Math.random() < 0.85) {
                    estadoActual[i][j] = 0;
                } else {
                    estadoActual[i][j] = 1;
                }
            }
        }
    }

    /**
     * Este método cuenta las células vecinas vivas que tiene una célula
     * @param i indica el número de fila de la célula
     * @param j indica el número de columna de la célula
     * @return devuelve el contador (número de células vecinas vivas)
     * 
     * Complejidad temporal: O(1)
     * Complejidad espacial: O(1)
     */
    public int contarCelulasVecinas(int i, int j){
        int contador = 0;
        if(estadoActual[i-1][j-1] == 1) contador++;
        if(estadoActual[i-1][j] == 1) contador++;
        if(estadoActual[i-1][j+1] == 1) contador++;
        if(estadoActual[i][j-1] == 1) contador++;
        if(estadoActual[i][j+1] == 1) contador++;
        if(estadoActual[i+1][j-1] == 1) contador++;
        if(estadoActual[i+1][j] == 1) contador++;
        if(estadoActual[i+1][j+1] == 1) contador++;
        return contador;
    }

    // La secuencia de ceros y unos generada es guardada en ‘estadoActual‘ y, utilizando las reglas del juego
    // de la vida, se insertan los ceros y unos correspondientes en ‘estadoSiguiente‘.
    /**
     * Transita al estado siguiente según las reglas del juego de la vida.
     * 
     * Complejidad temporal: O(n^2)
     * Complejidad espacial: O(1)
     */
    public void transitarAlEstadoSiguiente() {
        for(int i=1; i<DIMENSION-1; i++){
            for(int j=1; j<DIMENSION-1; j++){
                int contador = contarCelulasVecinas(i, j);
                estadoSiguiente[i][j] = 0;
                if(estadoActual[i][j] == 1){   //célula viva
                   if(contador == 2 || contador == 3) estadoSiguiente[i][j] = 1;
                }else{   //célula muerta
                    if(contador == 3) estadoSiguiente[i][j] = 1;
                }
            }
        }
        for(int i=1; i<DIMENSION-1; i++){
                estadoActual[i] = Arrays.copyOf(estadoSiguiente[i], DIMENSION);
        }
    }

    // La variable ‘estadoActual‘ pasa a tener el contenido de ‘estadoSiguiente‘ y, éste útimo 
    // atributo pasar a reflejar el siguiente estado.
    /**
     * Devuelve, en modo texto, el estado actual.
     * 
     * @return el estado actual.
     * 
     * Complejidad temporal: O(n^2)
     * Complejidad espacial: O(1)
     */
    @Override
    public String toString() {
        String t = "\n---MATRIZ---\n";
        for (int i = 0; i < DIMENSION; i++) {
            for (int j = 0; j < DIMENSION; j++) {
                t += estadoActual[i][j];
            }
            t += "\n";
        }
        return t;
    }
}
