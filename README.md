# JUEGO DE LA VIDA #

El propósito de este proyecto es obtener varias matrices en las que los 1's son células vivas y los 0's células muertas y, siguiendo una serie de reglas, las células mueren, reviven o se quedan igual.

### Reglas: ###

* Si una célula está viva y dos o tres de sus vecinas también lo están, entonces continúa viva en el estado siguiente.
* Si una célula está muerta y tres de sus vecinas están vivas, entonces pasa a estar viva en el estado siguiente.
* El resto de células pasan a estar muertas en el estado siguiente.

### Pre-requisitos ###

* Version 11 DE Open JDK
* Tener make instalado

### Compilación del programa ###

Para compilar el programa podemos ejecutar los siguientes comandos:

* make compilar
* make jar

### Ejecución del programa ###

* make ejecutar

### Generar Javadoc ###

* make javadoc

### Complejidad de los métodos ###

* método leerEstadoActual:  Complejidad temporal: O(n^2)
     						Complejidad espacial: O(1)

* método generarEstadoActualPorMontecarlo:  Complejidad temporal: O(n^2)
     										Complejidad espacial: O(1)

* método contarCelulasVecinas:  Complejidad temporal: O(1)
     							Complejidad espacial: O(1)
									
* método transitarAlEstadoSiguiente: Complejidad temporal: O(n^2)
     								 Complejidad espacial: O(1)
									 
* método toString:  Complejidad temporal: O(n^2)
     				Complejidad espacial: O(1)
					
* método main:  Complejidad temporal: O(n)
     			Complejidad espacial: O(1)

### Autores ###

Serena Blanco García